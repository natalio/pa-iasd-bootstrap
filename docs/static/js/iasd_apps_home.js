(function($){

	$(document).ready(function (){
		homeDSA.handleEvents();
		mainNav.handleEvents();
		mainNav.duplicateTouchLink();
		$(".apps-home-carousel").owlCarousel({
			// autoPlay: 7000,	
			addClassActive: true,
			mouseDrag: false,
			singleItem: true,
			slideSpeed: 700,
			touchDrag: false,
			transitionStyle: "fade"
		});		

		$('.apps-search').on('click', function(e) {
			var formId = $(this).data('target');
			var $form = $(formId);
			if ($form.is(":visible")) {
				$form.collapse('hide');
			} else {
				$form.collapse('show');
			}
		});
	});

	var deviceWidth = $(window).width();

	var homeDSA = {
		
		handleEvents : function(){
			$('.iasd-global_navbar-main .more, .iasd-global_navbar-main .navbar-toggle, .iasd-global_navbar-main .search-link').on('click', homeDSA.applyBackgroundImage);		
		},

		applyBackgroundImage : function(){
			if (deviceWidth >= 768) {
				var imageURL = $(".apps-home-carousel .active div").css('background-image');
				$('.iasd-global_navbar').css('background-image', imageURL);
				if( $('.iasd-global_navbar-more').hasClass('open') || $('.iasd-global_navbar-search').hasClass('open')){
					$('.apps-home-carousel .owl-controls').addClass('hidden');
					$(".apps-home-carousel").trigger('owl.stop');
				} else{
					$(".apps-home-carousel").trigger('owl.play',7000);
					$('.apps-home-carousel .owl-controls').removeClass('hidden');
				}
			}
		}
	};

	var isSmallDevice = window.matchMedia("(max-width: 768px)").matches; //Changes functionality on small devices

	var mainNav = {

		handleEvents : function(){
			$('.iasd-main_navbar .navbar-nav li > a').on('click', mainNav.preventDefaultTouchBehavior); // Fix events for large touch devices
			$('.iasd-main_navbar .navbar-toggle').on('click', mainNav.toggleResponsiveMenu);
		},

		preventDefaultTouchBehavior : function(e) {
			var jqThis = $(this).parent();
			if (!isSmallDevice) {
				jqThis.siblings().removeClass('open');
			}

			jqThis.toggleClass('open');

			if(jqThis.find('ul').length > 0 && (isSmallDevice || $('html').hasClass('touch')) ){
				e.preventDefault();
			}
            var aThis = jqThis.find('>:first-child');
            if(aThis)
                if(aThis.attr('href') == '#')
                    e.preventDefault();
		},

		duplicateTouchLink : function() {
			if(isSmallDevice || $('html').hasClass('touch')){
				$('.iasd-main_navbar .navbar-nav li.has-children').each(function(){
					var jqThis = $(this);
					var elementURL = jqThis.find('> a').attr('href');
					var elementContent = jqThis.find('> a').html();
					if(elementURL!='#'){
						jqThis.find('> ul').prepend('<li><a href="'+ elementURL +'" title="'+elementContent+'">'+elementContent+'</a></li>'); //TODO Usar jquery templates
					}
				});
			}
		},

		toggleResponsiveMenu : function(){
			$('.iasd-main_navbar .navbar-collapse').toggleClass('collapse').toggleClass('in');
		}

	};

})(jQuery);

